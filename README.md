# Welcome

## Information

**Cardboard Box** is a [Discord](https://discordapp.com) bot for my [**Discord server**](https://discord.gg/4zJ8xqV) powered by [Discord.js](https://discord.js.org). It comes packaged with many utilities and fun stuff, and even some special moderation functions.

**This bot is not suitable for usage outside of my Discord server!** This repository is primarily for issue tracking and global management between my own devices. If you really, _really_ want to run it yourself, read the sections below.

### Adding the bot to your guild

Don't want to run the bot yourself? Simply use [this link](https://discordapp.com/oauth2/authorize/?permissions=8&scope=bot&client_id=618576806177538079) to invite it to your own guild.

## Credits

* [SharpBot, a selfbot for Discord](https://github.com/RayzrDev/SharpBot) was actually my source for most of these cool commands! Huge kudos to [RayzrDev](https://github.com/RayzrDev) for making a cool selfbot.
* This was initially an edit to [GuideBot, the boilerplate example bot in Discord.js](https://github.com/AnIdiotsGuide/guidebot). It has some minor edits to the base code, but most of the basic stuff came from this very example bot.

## Join me

If you need a fun server to join, regardless of whether or not you're new to Discord, join now! If you've never used Discord before, don't fret! It only takes a few minutes to sign up and get started.

## [https://discord.gg/4zJ8xqV](https://discord.gg/4zJ8xqV)

